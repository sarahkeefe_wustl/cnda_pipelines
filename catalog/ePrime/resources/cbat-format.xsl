<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:cbat="http://nrg.wustl.edu/cbat"
xmlns:xnat="http://nrg.wustl.edu/xnat">

<xsl:output method="xml" indent="yes" />

<xsl:param name="project" />
<xsl:param name="visit_id" />
<xsl:param name="subject_ID" />
<xsl:param name="subject_Label" />
<xsl:param name="note" />

<xsl:template match="/">
	<xsl:variable name="exp"><xsl:value-of select="translate(events/params/value[@name='exp'], ' ', '')" /></xsl:variable>

	<xsl:variable name="rootElementName">
		<xsl:call-template name="getRootElementName">
			<xsl:with-param name="experimentType" select="$exp" />
		</xsl:call-template>
	</xsl:variable>
	
	<xsl:element name="{concat('cbat:', $rootElementName)}" namespace="http://nrg.wustl.edu/cbat" xmlns:xnat="http://nrg.wustl.edu/xnat">
		<xsl:attribute name="project"><xsl:value-of select="$project" /></xsl:attribute>
		<xsl:attribute name="visit_id"><xsl:value-of select="$visit_id" /></xsl:attribute>
		<xsl:attribute name="label">
			<xsl:call-template name="generateLabel">
				<xsl:with-param name="subjLabel" select="$subject_Label" />
				<xsl:with-param name="visit" select="$visit_id" />
				<xsl:with-param name="rootElementName" select="$rootElementName" />
			</xsl:call-template>
		</xsl:attribute>

		<xsl:variable name="sessDate"><xsl:value-of select="events/params/value[@name='sessDate']" /></xsl:variable>
		<xsl:element name="xnat:date">
			<xsl:call-template name="parseEPrimeDate">
				<xsl:with-param name="dateString" select="$sessDate" />
			</xsl:call-template>
		</xsl:element>
		
		<xsl:element name="xnat:time"><xsl:value-of select="events/params/value[@name='sessTime']" /></xsl:element>
		
		<xsl:element name="xnat:note"><xsl:value-of select="$note" /></xsl:element>
		<xsl:element name="xnat:subject_ID"><xsl:value-of select="$subject_ID" /></xsl:element>
		
		<xsl:call-template name="getTrials">
			<xsl:with-param name="experimentType" select="$exp" />
		</xsl:call-template>
	</xsl:element>
</xsl:template>

<xsl:template name="getRootElementName">
	<xsl:param name="experimentType" />
	
	<xsl:choose>
		<xsl:when test="contains($experimentType,'ReadSpan')">
			<xsl:text>ReadingSpan</xsl:text>
		</xsl:when>
		<xsl:when test="contains($experimentType,'AssocRGN')">
			<xsl:text>PairBinding</xsl:text>
		</xsl:when>
		<xsl:when test="contains($experimentType,'simon')">
			<xsl:text>Simon</xsl:text>
		</xsl:when>
      <xsl:when test="contains($experimentType,'Simon')">
         <xsl:text>Simon</xsl:text>
      </xsl:when>
		<xsl:when test="contains($experimentType,'CVandOEPureBlocks') or contains($experimentType,'CVOEswitchblock')">			
			<xsl:text>CVOE</xsl:text>
		</xsl:when>
		<xsl:when test="contains($experimentType,'SpatialRelationsB')">
			<xsl:text>VisualSpatialTest1</xsl:text>
		</xsl:when>
		<xsl:when test="contains($experimentType,'CompSpan')">
			<xsl:text>ComputationSpan</xsl:text>
		</xsl:when>
		<xsl:when test="contains($experimentType,'PaperFoldingB')">
			<xsl:text>VisualSpatialTest2</xsl:text>
		</xsl:when>
		<xsl:when test="contains($experimentType,'semvef')">
			<xsl:text>Categorization</xsl:text>
		</xsl:when>
      <xsl:when test="contains($experimentType,'SemVef')">
         <xsl:text>Categorization</xsl:text>
      </xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template name="generateLabel">
	<xsl:param name="subjLabel" />
	<xsl:param name="visit" />
	<xsl:param name="rootElementName" />
	
	<xsl:variable name="lcRootElementName" select="translate($rootElementName, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefjhijklmnopqrstuvwxyz')" />

	<xsl:value-of select="concat($subjLabel, '_', $visit, '_', $lcRootElementName)" />
</xsl:template>


<xsl:template name="parseEPrimeDate">
	<xsl:param name="dateString" />
	
	<xsl:variable name="YYYY" select="substring($dateString, 7, 4)" />
	<xsl:variable name="MM" select="substring($dateString, 1, 2)" />
	<xsl:variable name="DD" select="substring($dateString, 4, 2)" />
	
	<xsl:value-of select="concat($YYYY, '-', $MM, '-', $DD)" />
</xsl:template>

<xsl:template name="getTrials">
	<xsl:param name="experimentType" />
	
	<xsl:choose>
		<xsl:when test="contains($experimentType,'ReadSpan')">
			<xsl:call-template name="formatReadSpan" />
		</xsl:when>
		<xsl:when test="contains($experimentType,'AssocRGN')">
			<xsl:call-template name="formatPairBinding" />
		</xsl:when>
		<xsl:when test="contains($experimentType,'simon')">
			<xsl:call-template name="formatSimon" />
		</xsl:when>
      <xsl:when test="contains($experimentType,'Simon')">
         <xsl:call-template name="formatSimon" />
      </xsl:when>
		<xsl:when test="contains($experimentType,'CVandOEPureBlocks') or contains($experimentType,'CVOEswitchblock')">
			<xsl:call-template name="formatCvoe" />
		</xsl:when>
		<xsl:when test="contains($experimentType,'SpatialRelationsB')">
			<xsl:call-template name="formatSimple" />
		</xsl:when>
		<xsl:when test="contains($experimentType,'CompSpan')">
			<xsl:call-template name="formatSimple" />
		</xsl:when>
		<xsl:when test="contains($experimentType,'PaperFoldingB')">
			<xsl:call-template name="formatSimple" />
		</xsl:when>
		<xsl:when test="contains($experimentType,'semvef')">
			<xsl:call-template name="formatCategorization" />
		</xsl:when>
      <xsl:when test="contains($experimentType,'SemVef')">
         <xsl:call-template name="formatCategorization" />
      </xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template name="formatReadSpan">
	<xsl:for-each select="/events/event[value[@name='acc']]">
		<xsl:element name="{concat('cbat:T', position())}">
			<xsl:element name="cbat:accuracy"><xsl:value-of select="value[@name='acc']" /></xsl:element>
			<xsl:element name="cbat:response"><xsl:value-of select="value[@name='resp']" /></xsl:element>
		</xsl:element>
	</xsl:for-each>
</xsl:template>

<xsl:template name="formatPairBinding">
	<xsl:for-each select="/events/event[value[@name='cond']='Int']">
		<xsl:element name="{concat('cbat:IntactT', position())}">
			<xsl:element name="cbat:accuracy"><xsl:value-of select="value[@name='acc']" /></xsl:element>
		</xsl:element>
	</xsl:for-each>
	<xsl:for-each select="/events/event[value[@name='cond']='Rea']">
		<xsl:element name="{concat('cbat:MixedT', position())}">
			<xsl:element name="cbat:accuracy"><xsl:value-of select="value[@name='acc']" /></xsl:element>
		</xsl:element>
	</xsl:for-each>
	<xsl:for-each select="/events/event[value[@name='cond']='New']">
		<xsl:element name="{concat('cbat:NewT', position())}">
			<xsl:element name="cbat:accuracy"><xsl:value-of select="value[@name='acc']" /></xsl:element>
		</xsl:element>
	</xsl:for-each>
</xsl:template>

<xsl:template name="formatSimon">
	<xsl:for-each select="/events/event[value/@name='cong']">
		<xsl:element name="{concat('cbat:T', position())}">
			<xsl:element name="cbat:accuracy"><xsl:value-of select="value[@name='acc']" /></xsl:element>
			<xsl:element name="cbat:responseTime"><xsl:value-of select="value[@name='rt']" /></xsl:element>
			<xsl:element name="cbat:congruency"><xsl:value-of select="value[@name='cong']" /></xsl:element>
		</xsl:element>
	</xsl:for-each>
</xsl:template>

<xsl:template name="formatCvoe">
	<xsl:for-each select="/events/event[value/@name='cvacc']">
		<xsl:element name="{concat('cbat:CVT', position())}">
			<xsl:element name="cbat:accuracy"><xsl:value-of select="value[@name='cvacc']" /></xsl:element>
			<xsl:element name="cbat:responseTime"><xsl:value-of select="value[@name='cvrt']" /></xsl:element>
		</xsl:element>
	</xsl:for-each>
	<xsl:for-each select="/events/event[value/@name='oeacc']">
		<xsl:element name="{concat('cbat:OET', position())}">
			<xsl:element name="cbat:accuracy"><xsl:value-of select="value[@name='oeacc']" /></xsl:element>
			<xsl:element name="cbat:responseTime"><xsl:value-of select="value[@name='oert']" /></xsl:element>
		</xsl:element>
	</xsl:for-each>
	<xsl:for-each select="/events/event[value/@name='acc']">
		<xsl:element name="{concat('cbat:CVOET', position())}">
			<xsl:element name="cbat:accuracy"><xsl:value-of select="value[@name='acc']" /></xsl:element>
			<xsl:element name="cbat:responseTime"><xsl:value-of select="value[@name='rt']" /></xsl:element>
		</xsl:element>
	</xsl:for-each>
</xsl:template>

<xsl:template name="formatSimple">
	<xsl:for-each select="/events/event[value[@name='acc']]">
		<xsl:element name="{concat('cbat:T', position())}">
			<xsl:element name="cbat:accuracy"><xsl:value-of select="value[@name='acc']" /></xsl:element>
		</xsl:element>
	</xsl:for-each>
</xsl:template>


<xsl:template name="formatCategorization">
	<xsl:for-each select="/events/event[value[@name='rel']='cat']">
		<xsl:element name="{concat('cbat:YesT', position())}">
			<xsl:element name="cbat:accuracy"><xsl:value-of select="value[@name='acc']" /></xsl:element>
			<xsl:element name="cbat:responseTime"><xsl:value-of select="value[@name='rt']" /></xsl:element>
		</xsl:element>
	</xsl:for-each>
	<xsl:for-each select="/events/event[value[@name='rel']='assoc']">
		<xsl:element name="{concat('cbat:NoT', position())}">
			<xsl:element name="cbat:accuracy"><xsl:value-of select="value[@name='acc']" /></xsl:element>
			<xsl:element name="cbat:responseTime"><xsl:value-of select="value[@name='rt']" /></xsl:element>
		</xsl:element>
	</xsl:for-each>
</xsl:template>

</xsl:stylesheet>
