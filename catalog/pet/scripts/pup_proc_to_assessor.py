#!/usr/bin/python

from lxml.builder import ElementMaker
from lxml.etree import tostring as xmltostring
import datetime as dt
import sys, json, argparse

versionNumber='9'
dateString='20150220'
author='flavin'
progName=sys.argv[0].split('/')[-1]
idstring = '$Id: %s,v %s %s %s Exp $'%(progName,versionNumber,dateString,author)

nsdict = {'xnat':'http://nrg.wustl.edu/xnat',
          'xsi':'http://www.w3.org/2001/XMLSchema-instance',
          'pup':'http://nrg.wustl.edu/pup'}
def ns(namespace,tag):
    return "{%s}%s"%(nsdict[namespace],tag)

def schemaLoc(namespace):
    return "{0} https://cnda.wustl.edu/schemas/{1}/{1}.xsd".format(nsdict[namespace],namespace)

def main():
    #######################################################
    # PARSE INPUT ARGS
    parser = argparse.ArgumentParser(description='Generate XML assessor file from PUP output logs')
    parser.add_argument('-v', '--version',
                        help='Print version number and exit',
                        action='version',
                        version=versionNumber)
    parser.add_argument('--idstring',
                        help='Print id string and exit',
                        action='version',
                        version=idstring)
    parser.add_argument('-p','--paramsFile',
                        help='Bash-formatted params file')
    parser.add_argument('-e','--errorFile',
                        help='JSON file containing motion correction and registration errors')
    parser.add_argument('-s','--statsFile',
                        help='JSON file containing ROI names and statistics')
    parser.add_argument('-o','--XMLFilePath',
                        help='Path to XML assessor file (output)')
    args=parser.parse_args()
    #######################################################

    #######################################################
    # READ PARAMS FILE
    with open(args.paramsFile,'r') as f:
        paramsDict = dict( [line.strip().split('=') for line in f.readlines()] )
    #######################################################

    #######################################################
    # ASSEMBLE HEADERS AND OTHER NECESSARY INFO
    sessionId = paramsDict["sessionId"]

    datestamp = paramsDict["datestamp"]
    if len(datestamp) >= 8:
        date = '{}-{}-{}'.format(datestamp[0:4],datestamp[4:6],datestamp[6:8])
    else:
        date = dt.date.today().isoformat()

    assessorElementsDict = {
        "userfullname":paramsDict["userfullname"],
        "petLabel":paramsDict["label"],
        "petScanId":paramsDict["pet_scan_id"],
        "procType":paramsDict["proc_type_pretty"],
        "model":paramsDict["model"],
        "tracer":paramsDict["tracer"],
        "halfLife":paramsDict["half_life"],
        "suvrFlag":paramsDict["suvr"],
        "refroistr":paramsDict["refroistr"],
        "delayParam":paramsDict["delay"],
        "mst":paramsDict["mst"],
        "mdt":paramsDict["mdt"],
        "tbl":paramsDict["tbl"],
        "rbf":paramsDict["rbf"],
        "mbf":paramsDict["mbf"],
        "sf":paramsDict["sf"],
        "ef":paramsDict["ef"],
        "filter":paramsDict["filter"],
        "fwhm":paramsDict["fwhm"],
        "pvc2cflag":paramsDict["pvc2cflag"],
        "rsfflag":paramsDict["rsfflag"],
    }

    if paramsDict.get("template"):
        assessorElementsDict['templateType'] = paramsDict["template"]
    if paramsDict.get("fsid"):
        assessorElementsDict['FSId'] = paramsDict["fsid"]
        assessorElementsDict['t1'] = paramsDict["t1"]
    if paramsDict.get("mrId"):
        assessorElementsDict['MRId'] = paramsDict["mrId"]
    if paramsDict.get("filter")=="1":
        assessorElementsDict["filterxy"] = paramsDict["filterxy"]
        assessorElementsDict["filterz"] = paramsDict["filterz"]

    assessorTitleAttributesDict = {'ID':paramsDict["assessorId"], 'project':paramsDict["project"], 'label':paramsDict["assessorLabel"],
                ns('xsi','schemaLocation'):' '.join(schemaLoc(namespace) for namespace in ('xnat','pup'))}
    #######################################################

    #######################################################
    # READ ROI STAT JSON FILE
    # JSON (and resulting dict) will be of the form {ROIName:{StatTitle:StatNumberValue,...},...}
    with open(args.statsFile,'r') as f:
        statsDict = json.loads(f.read())
    #######################################################

    #######################################################
    # READ ERROR JSON FILE
    # Must contain list of motion correction errors
    # May also contain PET registration error
    with open(args.errorFile,'r') as f:
        errorDict = json.loads(f.read())

    if 'moco_error_list' in errorDict and errorDict['moco_error_list']:
        assessorElementsDict['mocoError'] = str(max(errorDict['moco_error_list']))
    # else:
    #    sys.exit('%s ERROR: No/Empty motion correction error list in %s'%(progName,args.errorFile))

    if 'registration_error' in errorDict:
        assessorElementsDict['regError'] = str(errorDict['registration_error'])
    #######################################################

    #######################################################
    # BUILD ASSESSOR XML
    # Building XML using lxml ElementMaker.
    # For documentation, see http://lxml.de/tutorial.html#the-e-factory
    E = ElementMaker(namespace=nsdict['pup'],nsmap=nsdict)
    assessorXML = E('PUPTimeCourse', assessorTitleAttributesDict,
        E(ns('xnat','date'),date),
        # E(ns('xnat','fields'),
        #     *[E(ns('xnat','field'),{'name':fieldName},fieldVal) for fieldName,fieldVal in fields.iteritems()]
        #     ),
        E(ns('xnat','imageSession_ID'),sessionId),
        E('rois',
            *[E('roi', {'name':roiName},
                *[E(statTitle,stat) for statTitle,stat in roiDict.iteritems()]
                ) for roiName,roiDict in statsDict.iteritems()]
            ),
        *[E(elName,elValue) for elName,elValue in assessorElementsDict.iteritems()]
        )

    print 'Writing assessor XML to %s'%args.XMLFilePath
    # print(xmltostring(assessorXML, pretty_print=True))
    with open(args.XMLFilePath,'w') as f:
        f.write(xmltostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True))


if __name__ == '__main__':
    print idstring
    main()
