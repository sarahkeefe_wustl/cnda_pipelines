function tlv(pmap)

spm('defaults', 'FMRI');
spm_jobman('initcfg');
clear matlabbatch;

matlabbatch{1}.spm.tools.LST.lesvolume.data_plm_thresh = {strcat(pmap,',1')};
matlabbatch{1}.spm.tools.LST.lesvolume.adiof = 0;

spm_jobman('run', matlabbatch)
