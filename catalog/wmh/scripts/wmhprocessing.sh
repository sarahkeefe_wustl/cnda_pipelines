#!/bin/bash -e
VERSION="1.2"


if [ "$1" == "--version" ]; then
    echo $VERSION
    exit 0
fi

die(){
    echo >&2 "$@"
    echo "$@"
    exit 1
}

paramsfile=$1
source $paramsfile

dir=$(pwd)

echo "WMH Preprocessing"

if [[ "${t1_is_dicom}" == "true" ]]; then
    t1DcmFile=$(ls ${t1dir} | sort -u | head -1 2> /dev/null)
    echo
    echo "Converting t1 to nifti"
    echo "Directory: ${t1dir}"
    echo "File: ${t1DcmFile}"
    echo
    dcm2niix -b n -f %i -z y ${t1dir}/${t1DcmFile} || die "ERROR: Could not convert T1 to nifti"
fi
if [[ "${flair_is_dicom}" == "true" ]]; then
    flairDcmFile=$(ls ${flairdir} | sort -u | head -1 2> /dev/null)
    echo
    echo "Converting flair to nifti"
    echo "Directory: ${flairdir}"
    echo "File: ${flairDcmFile}"
    echo
    dcm2niix -b n -f %i -z y ${flairdir}/${flairDcmFile} || die "ERROR: Could not convert FLAIR to nifti"
fi

t1File=$(find ${t1dir} -name '*.nii' | head -1)
echo
echo "T1 nii file: ${t1File}"
if [[ -z $t1File ]]; then
    echo "Not found"
    echo "Searching for T1 nii.gz file"
    t1NiigzFile=$(find ${t1dir} -name '*.nii.gz' | head -1)
    echo "T1 nii.gz file: ${t1NiigzFile}"
    if [[ -z $t1NiigzFile ]]; then
        echo "Not found"
    else
        echo "Un-gzipping"
        pushd ${t1dir}
        fslchfiletype NIFTI $t1NiigzFile
        popd

        t1File=$(find ${t1dir} -name '*.nii' | head -1)
        echo "T1 nii file: ${t1File}"
    fi

    if [[ -z $t1File ]]; then
        die "ERROR: Could not find t1 nifti file"
    fi
fi

flairFile=$(find ${flairdir} -name '*.nii' | head -1)
echo
echo "FLAIR nii file: ${flairFile}"
if [[ -z $flairFile ]]; then
    echo "Not found"
    echo "Searching for FLAIR nii.gz file"
    flairNiigzFile=$(find ${flairdir} -name '*.nii.gz' | head -1)
    echo "FLAIR nii.gz file: ${flairNiigzFile}"
    if [[ -z $flairNiigzFile ]]; then
        echo "Not found"
    else
        echo "Un-gzipping"
        pushd ${flairdir}
        fslchfiletype NIFTI $flairNiigzFile
        popd

        flairFile=$(find ${flairdir} -name '*.nii' | head -1)
        echo "FLAIR nii file: ${flairFile}"
    fi

    if [[ -z $flairFile ]]; then
        die "ERROR: Could not find flair nifti file"
    fi
fi

t1Name=`echo ${t1File} | rev | cut -d/ -f1 | rev`
flairName=`echo ${flairFile} | rev | cut -d/ -f1 | rev`

echo
echo "Reorienting to standard"
mkdir -p ${datadir}/std/${t1_scan}
mkdir -p ${datadir}/std/${flair_scan}
fslreorient2std ${t1File} ${datadir}/std/${t1_scan}/${t1Name}
fslreorient2std ${flairFile} ${datadir}/std/${flair_scan}/${flairName}

stdT1File=$(find ${datadir}/std/${t1_scan} -name '*.nii' | head -1)
echo
echo "Std T1 nii file: ${stdT1File}"
if [[ -z $stdT1File ]]; then
    echo "Not found"
    echo "Searching for Std T1 nii.gz file"
    stdT1NiigzFile=$(find ${datadir}/std/${t1_scan} -name '*.nii.gz' | head -1)
    echo "Std T1 nii.gz file: ${stdT1NiigzFile}"
    if [[ -z $stdT1NiigzFile ]]; then
        echo "Not found"
    else
        echo "Un-gzipping"
        pushd ${datadir}/std/${t1_scan}
        fslchfiletype NIFTI $stdT1NiigzFile
        popd

        stdT1File=$(find ${datadir}/std/${t1_scan} -name '*.nii' | head -1)
        echo "Std T1 nii file: ${stdT1File}"
    fi

    if [[ -z $stdT1File ]]; then
        die "ERROR: Could not find t1 nifti file after reorienting to std"
    fi
fi

stdFlairFile=$(find ${datadir}/std/${flair_scan} -name '*.nii' | head -1)
echo
echo "Std FLAIR nii file: ${stdFlairFile}"
if [[ -z $stdFlairFile ]]; then
    stdFlairNiigzFile=$(find ${datadir}/std/${flair_scan} -name '*.nii.gz' | head -1)
    echo "Not found"
    echo "Searching for Std FLAIR nii.gz file"
    if [[ -z $stdFlairNiigzFile ]]; then
        echo "Not found"
    else
        echo "Un-gzipping"
        pushd ${datadir}/std/${flair_scan}
        fslchfiletype NIFTI $stdFlairNiigzFile
        popd

        stdFlairFile=$(find ${datadir}/std/${flair_scan} -name '*.nii' | head -1)
        echo "T1 nii file: ${stdFlairFile}"
    fi

    if [[ -z $stdFlairFile ]]; then
        die "ERROR: Could not find flair nifti file after reorienting to std"
    fi
fi

echo
echo "Running WMH processing"
#matlab -nosplash -nodisplay << END || die "WMH processing failed"
# -r "\""addpath(genpath('/data/CNDA/pipeline/catalog/wmh/resources/matlab'));
#addpath('${SPMPATH}');
#addpath('${LSTPATH}');

#LST '${stdT1File}' '${stdFlairFile}';

#exit"\""
#END

matlab -nosplash -nodisplay -r "addpath('${SPMPATH}'); addpath('${MATLABPATH}'); addpath('${LSTPATH}'); LST '${stdT1File}' '${stdFlairFile}' ; exit" ||  die "WMH processing failed"

echo
echo "Running WMH postprocessing"
echo "Pulling stats from $(ls ${datadir}/std/${t1_scan}/b_000*)"
fslstats ${datadir}/std/${t1_scan}/b_000* -V | cut -d' ' -f1,2 | tee fslstats_b_000_measures.txt

echo
echo "Writing assessor XML"
python ${SCRIPTPATH}/wmhpostproc.py ${project} ${id} ${assessorId} ${assessorLabel} ${assessorXMLPath} ${datestamp} ${t1_scan} ${flair_session_resolved} ${flair_scan} fslstats_b_000_measures.txt

echo
echo "Making snapshots with slicer"
slicer ${datadir}/std/${flair_scan}/rm*.nii -l Greyscale ${datadir}/std/${t1_scan}/b_000* -l Red -S 4 2000 ${label}_snapshot.png

echo
echo "gzipping nii files"
fslchfiletype NIFTI_GZ ${datadir}/std/${t1_scan}/b_000*.nii
fslchfiletype NIFTI_GZ ${datadir}/std/${flair_scan}/rm*.nii

echo "Done"
echo
