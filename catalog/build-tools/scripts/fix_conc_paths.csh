#!/bin/csh -x
#Changes the path from builddir to archivedir
#Invoke from PROCESSED/BOLD folder

set builddir = $1
set archivedir = $2


pushd FCmaps

foreach f (*.conc)
        sed -e  's%'$builddir'%'$archivedir'%' $f >! temp$$
        /bin/mv temp$$ $f
end
