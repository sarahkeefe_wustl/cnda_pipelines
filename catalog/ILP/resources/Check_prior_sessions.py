#!/bin/env python
import sys, json, argparse, requests, datetime, csv, os, dicom
from xml.etree import cElementTree as ElementTree
from dateutil.relativedelta import relativedelta

# This script takes a scan's accession number and returns two csvs, one containing all the patient's sessions and another containing their ages at the time of each session.


# parse arguments to the script
parser = argparse.ArgumentParser(description='Create a list of sessions and ages to be used by FS_stats_maker.sh.')
parser.add_argument('session_id',help="MR session id")
parser.add_argument('host',help="host")
parser.add_argument('user',help="username")
parser.add_argument('pwd',help="password")
args=parser.parse_args()

# session_id will be used to find the subject ID
session_id = args.session_id
host = args.host
user = args.user
pwd = args.pwd

# set namespace for ElementTree
namespaces = {'xnat': 'http://nrg.wustl.edu/xnat', 'fs':'http://nrg.wustl.edu/fs'}


# check_dicoms_for_age()
# Download and check DICOM files for the MR scan linked to the Freesurfer.
def check_dicoms_for_age(experiment, session_number, assessor_id, session_date):
	# check dicom files for patient birthdate or age, in case xnat:dcmPatientBirthDate doesn't exist
	# get scan id used by the Freesurfer
	subject_age = None
	subject_birthdate = None

	scan_id = None
	assessor_url = host + 'data/experiments/' + session_number + '/assessors/' + assessor_id + '?format=xml'

	scans_request = requests.get(assessor_url, auth=(user, pwd), verify=False)
	scans_root = ElementTree.fromstring(scans_request.text)

	# go through parameters and look for the INCLUDED_T1 parameter to use
	if scans_root.find('xnat:parameters',namespaces) is not None:
		for params in scans_root.find('xnat:parameters',namespaces):
			#params_list = scans_result.find('xnat:parameters', namespaces)
			if params is not None:
				if params.attrib["name"] == 'INCLUDED_T1':
					scan_id = params.text.strip(os.linesep)

		# check that we got the scan ID
		if scan_id is not None:
			file_name = None
			files_url = host + 'data/experiments/' + session_number + '/scans/' + scan_id + '/resources/DICOM/files?format=json'
			files_request = requests.get(files_url, auth=(user, pwd), verify=False)
			files_json = json.loads(files_request.text)
			try:
				file_name = files_json['ResultSet']['Result'][0]['Name']
			except:
				print("Could not get scan filename from the request, and we needed it to get the age for this session.")


			# get the URL for a dicom file in the scan used by the Freesurfer
			# request the dicom file
			if file_name is not None:
				dicom_request_url = host + 'REST/experiments/' + session_number + '/scans/' + scan_id + '/resources/DICOM/files/RAW/' + file_name
				dicom_request = requests.get(dicom_request_url, auth=(user, pwd), verify=False, stream=True)

				# save the dicom file
				try:
					with open(session_number + '_scan' + scan_id + '_sampledicom.dcm', 'wb') as handle:
						for block in dicom_request.iter_content(1024):
							handle.write(block)

					# use pydicom to extract the age or date of birth from the dicom file				
					scan_dicom = dicom.read_file(session_number + '_scan' + scan_id + '_sampledicom.dcm', force=True)
					if "PatientAge" in scan_dicom:
						dcm_age = scan_dicom.PatientAge
						# this is in the format "078Y"
						dcm_age_split = dcm_age.split("Y")
						subject_age = int(dcm_age_split[0])
					if "PatientBirthDate" in scan_dicom:
						dcm_birthdate = scan_dicom.PatientBirthDate
						# this is in the format "19700101"
						subject_birthdate = datetime.datetime.strptime(dcm_birthdate, "%Y%m%d")
				except:
					print("There was a problem reading the DICOM file. Continuing...")
			else:
				print("Could not get a valid filename for a DICOM file to check for age. Continuing...")
		else:
			print("Could not get INCLUDED_T1 scan ID from the Freesurfer data. Continuing...")
	else:
		print("Could not get scan parameters. Continuing...")

	return subject_age, subject_birthdate


# get_subject_age()
# Check for subject birthdate in the XNAT XML. If that isn't there,
# download and check DICOM files for the MR scan linked to the Freesurfer (using check_dicoms_for_age()).
# Once we have age or birthdate, calculate and return the age.
def get_subject_age(experiment, subject_data, session_number, assessor_id, session_date):
	subject_birthdate = None
	subject_age = None

	# get subject birthdate - check xnat:dcmPatientBirthDate field (default in TIP)
	# if not there, check the Subject/xnat:demographics/dob
	if experiment.find('xnat:dcmPatientBirthDate', namespaces) is not None and experiment.find('xnat:dcmPatientBirthDate', namespaces).text is not None and experiment.find('xnat:dcmPatientBirthDate', namespaces).text is not "":
		subject_birthdate = datetime.datetime.strptime(experiment.find('xnat:dcmPatientBirthDate', namespaces).text, "%Y-%m-%d")
	elif subject_data.findall('.//xnat:dob', namespaces) is not None and len(subject_data.findall('.//xnat:dob', namespaces)) > 0 and subject_data.findall('.//xnat:dob', namespaces)[0].text is not None and subject_data.findall('.//xnat:dob', namespaces)[0].text is not "":
		subject_birthdate = datetime.datetime.strptime(subject_data.findall('.//xnat:dob', namespaces)[0].text, "%Y-%m-%d")
	elif subject_data.findall('.//xnat:yob', namespaces) is not None and len(subject_data.findall('.//xnat:yob', namespaces)) > 0 and subject_data.findall('.//xnat:yob', namespaces)[0].text is not None and subject_data.findall('.//xnat:yob', namespaces)[0].text is not "":
		subject_birthyear = subject_data.findall('.//xnat:yob', namespaces)[0].text
		subject_birthdate = datetime.datetime.strptime(subject_birthyear + '-01-01', "%Y-%m-%d")
	else:
		subject_age, subject_birthdate = check_dicoms_for_age(experiment, session_number, assessor_id, session_date)

	if subject_age is None and subject_birthdate is not None:
		# calculate age at session date based on session date and birthdate							
		return relativedelta(session_date, subject_birthdate).years
	else:
		return subject_age



print("Session ID: " + session_id)

# get subjectID out of xml result

session_url = host + 'data/experiments/' + session_id + '?format=xml'
print session_url

r = requests.get(session_url, auth=(user, pwd), verify=False)
session_root = ElementTree.fromstring(r.text)
session_label = session_root.attrib["label"]
subject_id = session_root.find('xnat:subject_ID', namespaces).text


# check that we got a subject ID; if we did, then use it to construct a list of ages and sessions
# for past freesurfers in other experiments linked to this subject
if subject_id is not None:

	# obtain sessions and session dates using subject_id
	subject_url = host + 'data/subjects/' + subject_id + '?format=xml'
	print "Checking URL for subject data: " + subject_url

	r2 = requests.get(subject_url, auth=(user, pwd), verify=False)
	subject_data = ElementTree.fromstring(r2.text.encode('ascii', 'ignore'))

	session_list = []
	age_list = []

	experiment_list = subject_data.find('xnat:experiments', namespaces)

	if experiment_list is not None:

		# go through the list of experiments that we got from our request
		for experiment in experiment_list:

			# get the experiment ID and type of experiment
			# we only care about MR sessions or PET MR sessions
			if experiment is not None and ((experiment.attrib['{http://www.w3.org/2001/XMLSchema-instance}type'] == "xnat:mrSessionData") or (experiment.attrib['{http://www.w3.org/2001/XMLSchema-instance}type'] == "xnat:petSessionData")):
				
				experiment_id = experiment.attrib['ID']
				mr_label = experiment.attrib['label']

				# get the list of assessors for this experiment
				# we want to look for Freesurfers in this assessor list
				assessor_list = experiment.find('xnat:assessors', namespaces)
				if assessor_list is not None:

					freesurfers_list = []

					for assessor in assessor_list:
						# check each assessor in the list
						print ("Going through assessors for experiment " + experiment_id)

						assessor_id = assessor.attrib['ID']
						assessor_type = assessor.attrib['{http://www.w3.org/2001/XMLSchema-instance}type']
						assessor_date = assessor.find('xnat:date', namespaces).text

						print("Found assessor: type " + assessor_type + "; id " + assessor_id)

						# check validation status and omit it if the freesurfer failed
						qc_status_object = assessor.find('xnat:validation', namespaces)
						qc_status = qc_status_object.attrib.get('status', "") if qc_status_object is not None else False
						
						fs_version = None
						fs_version_obj = assessor.find('fs:fs_version', namespaces)
						if fs_version_obj is not None:
							fs_version = fs_version_obj.text
						print(fs_version)
						# only include a Freesurfer in the ILP graphs if it has been QC validated, and has passed or passed with edits.
						if (assessor_type == "fs:fsData") and qc_status and ((qc_status == "Passed") or (qc_status == "Passed with edits")) and fs_version and (("v5.3" in fs_version) or ("v6.0" in fs_version)):

							session_number = experiment.attrib['ID']

							# Date of session - get this from the experiment. 
							# We want the MR date, not the assessor date
							session_date = datetime.datetime.strptime(experiment.find('xnat:date', namespaces).text, "%Y-%m-%d")

							# get the subject age from the experiment data, subject data, or dicom
							# send the subject information too because the dob might be in there.
							subject_age = get_subject_age(experiment, subject_data, session_number, assessor_id, session_date)

							if subject_age is None:
								print ("Could not get subject age for assessor " + assessor_id + ". Continuing...")
							else:
								# If we got an age and a session number, add it to the freesurfer list
								# this will be a list of all valid Freesurfers we could potentially use in the graph
								# put in mr_label to use for getting the aseg stats files
								# put in assessor date so we can sort by assessor date and use the most recent one
								one_freesurfer = (subject_age, session_number, assessor_id, mr_label, assessor_date)
								freesurfers_list.append(one_freesurfer)

					print("Freesurfer assessors list: " + str(freesurfers_list))

					if len(freesurfers_list) > 0:
						freesurfers_list.sort(key=lambda tup: tup[4])
						most_recent_fs = freesurfers_list[-1]

						fs_age_at_session = most_recent_fs[0]
						fs_session_number = most_recent_fs[1]
						fs_assessor_id = most_recent_fs[2]
						fs_mr_label = most_recent_fs[3]
						fs_assessor_date = most_recent_fs[4]

						if ((len(age_list)) > 0 and (age_list[-1] == fs_age_at_session)):
							print("Found a session where the patient is the same age as a previous session. Overwriting with the more recent session.")
							age_list[-1] = fs_age_at_session
							session_list[-1] = fs_mr_label
						else:
							print("Adding session " + fs_mr_label + " (Age " + str(fs_age_at_session) + ") to the list.")
							age_list.append(fs_age_at_session)
							session_list.append(fs_mr_label)

						# Grab the aseg.stats, lh.aparc.stats and rh.aparc.stats files
						# Create the directory if it doesn't exist
						print("Getting aseg.stats, lh.aparc.stats, and rh.aparc.stats files for this session.")
						if not os.path.isdir(fs_mr_label + '/stats'):
							os.makedirs(fs_mr_label + '/stats')

						# We want to get the files and overwrite them if they exist
						# construct the url to grab files from REST call
						url = host + 'data/experiments/' + fs_session_number + '/assessors/' + fs_assessor_id + '/resources/DATA/files/' + fs_mr_label + '/stats/'
						# get aseg.stats: write resulting files to new file in directory
						with open(os.path.join(fs_mr_label + '/stats', 'aseg.stats'), "w") as file1:
							toFile = requests.get(url + 'aseg.stats', auth=(user, pwd), verify=False)
							file1.write(toFile.text)
						# get lh.aparc.stats: write resulting files to new file in directory
						with open(os.path.join(fs_mr_label + '/stats', 'lh.aparc.stats'), "w") as file2:
							toFile = requests.get(url + 'lh.aparc.stats', auth=(user, pwd), verify=False)
							file2.write(toFile.text)
						# get rh.aparc.stats: write resulting files to new file in directory
						with open(os.path.join(fs_mr_label + '/stats', 'rh.aparc.stats'), "w") as file3:
							toFile = requests.get(url + 'rh.aparc.stats', auth=(user, pwd), verify=False)
							file3.write(toFile.text)
					else:
						print("Couldn't find any Freesurfers for this session. Continuing...")

		if len(age_list) > 0 and len(session_list) > 0:

			print("Writing age list and session list files.")

			# write ages to csv file for use in FS_stats_maker
			with open(session_id + '_age_list.csv', 'wb') as fp:
			    a = csv.writer(fp, delimiter='\t')
			    a.writerow(["Age"])
			    data = age_list
			    for i in data:
			    	a.writerow([i])

			# write session list to csv file for use in FS_stats_maker
			with open(session_id + '_session_list.csv', 'wb') as fp:
			    a = csv.writer(fp, delimiter=',')
			    #a.writerow(["Session"]) # don't need this - freesurfer needs session #'s only
			    data = session_list
			    for i in data:
			    	a.writerow([i])
		else:
			raise ValueError("Couldn't find any Freesurfer assessors with ages for any sessions for this subject!")

	else:
		raise ValueError("Could not find experiments for this subject.")
else:
	raise ValueError("Could not find a Subject ID from the given session ID (" + session_id + ").")
