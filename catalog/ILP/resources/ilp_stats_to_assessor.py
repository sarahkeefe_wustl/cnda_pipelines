#!/bin/env python
import os, sys, csv, argparse
from lxml import etree

versionNumber='1'
dateString='20150617'
author='flavin'
progName=os.path.basename(sys.argv[0])
idstring = '$Id: %s,v %s %s %s $'%(progName,versionNumber,dateString,author)

# Parse input args
parser = argparse.ArgumentParser(description='Write ILP stats to FreeSurfer assessor XML.')
parser.add_argument('-v', '--version',
                    help='Print version number and exit',
                    action='version',
                    version=versionNumber)
parser.add_argument('--idstring',
                    help='Print id string and exit',
                    action='version',
                    version=idstring)
parser.add_argument('age',help='The age parameter used during ILP processing.')
parser.add_argument('FSXML',help="Path to existing FreeSurfer assessor XML.")
parser.add_argument('ilpStatsFile',help='The ILP stats file')
parser.add_argument('FSROICSV',help='The CSV file that encodes the mapping between FS ROI names and ILP column names.')
parser.add_argument('outputXML',help='Output ILP assessor XML.')
parser.add_argument('--debug',action='store_true')
args=parser.parse_args()


# Read csvs. Build some sort of object with them
print "Reading ILP stats file: "+args.ilpStatsFile
with open(args.ilpStatsFile) as f:
    ilp = [row for row in csv.reader(f)]
print "Done\n"

print "Reading Freesurfer/ILP ROI mapping csv: "+args.FSROICSV
with open(args.FSROICSV) as f:
    rois = [row for row in csv.reader(f)]
print "Done\n"

# Before processing with ILP, we had renamed the FS ROIs to more tractable things.
# This line reverses those changes.
roismap = {roi[1] : roi[0].lstrip('_').replace('_','-').replace('third','3rd').replace('fourth','4th').replace('fifth','5th').replace("IntraCranialVol","EstimatedTotalIntraCranialVol") for roi in rois[1:]}

# Read FS XML
print "Reading Freesurfer assessor XML: "+args.FSXML
fsxml = etree.parse(args.FSXML)
print "Done\n"
fsroot = fsxml.getroot()
fsns = fsroot.nsmap["fs"]

existingAgeEl = fsroot.xpath('fs:ilp_age', namespaces=fsroot.nsmap)
if existingAgeEl:
    oldage = existingAgeEl[0].text
    existingAgeEl[0].text = args.age
    print "Overwriting existing 'age' %s with input value %s"%(oldage,args.age)
else:
    print "Adding 'age' parameter (value=%s) to assessor"%args.age
    etree.SubElement(fsroot, "{%s}ilp_age"%fsns).text = args.age
print

if len(existingAgeEl) > 1:
    for extraAge in existingAgeEl[1:]:
        fsroot.remove(extraAge)
        if args.debug: print "Removing superfluous 'age' element"

volumeXpath = 'fs:measures/fs:volumetric/fs:regions/fs:region[@name=$roiname]'
surfaceXpath = 'fs:measures/fs:surface/fs:hemisphere[@name=$leftRight]/fs:regions/fs:region[@name=$roiname]'
# fsrois = fsroot.xpath('/fs:measures/fs:volumetric/fs:regions', namespaces=fsroot.nsmap)

print "Adding ILP stats to Freesurfer assessor..."
for ilpRoi,stat in zip(*ilp):
    if args.debug: print "ILP ROI '%s'"%ilpRoi

    if ilpRoi == 'Session' or ilpRoi == 'Age':
        if args.debug: print "Skipping 'Session' and 'Age'"
        continue

    if stat == 'NA':
        if args.debug:
            print "Replacing NA with NaN. See issues.xnat.org ticket PIP-110."
        stat = 'NaN'
    elif not (stat == 'Inf' or stat == '-Inf' or stat == 'NaN'):
        if args.debug:
            print "Truncating stat decimal places. {0}->{0:.8f}".format(float(stat))
        stat = "{0:.8f}".format(float(stat))

    zscore = ilpRoi.endswith('zscore')
    if zscore:
        ilpRoi = ilpRoi.replace('_zscore','')
        if args.debug: print "Stripping '_zscore'. ILP ROI is '%s'"%ilpRoi

    if ilpRoi.startswith('left'):
        leftRight = "left"
        leftRightName = "Left"
        if args.debug and leftRight: print "ILP ROI '%s' is a %s stat"%(ilpRoi,leftRight)

        ilpRoiSearch = ilpRoi
    elif ilpRoi.startswith('right'):
        leftRight = "right"
        leftRightName = "Right"
        if args.debug and leftRight: print "ILP ROI '%s' is a %s stat"%(ilpRoi,leftRight)


        ilpRoiSearch = ilpRoi.replace('right','left')
        if args.debug: print "There aren't FS ROI names for right ILP stats for some reason."
        if args.debug: print "Searching instead for an FS ROI using ILP ROI name '%s'"%ilpRoiSearch
    else:
        leftRight = ""

    if ilpRoiSearch not in roismap:
        sys.exit("Could not find a mapping for ILP ROI '%s'\nExiting"%ilpRoi)

    fsRoi = roismap[ilpRoiSearch]
    if not fsRoi:
        sys.exit("ILP ROI '%s' maps to a blank value\nExiting"%ilpRoi)
    if args.debug: print "Maps to '%s'"%fsRoi

    isSurf = '_surfacemeasure_' in ilpRoi
    if args.debug:
        if isSurf:
            print "Assuming ILP ROI '%s' is a surface measure"%ilpRoi
        else:
            print "Assuming ILP ROI '%s' is a volumetric measure"%ilpRoi


    if not isSurf:
        fsRoiElList = fsroot.xpath(volumeXpath,roiname=fsRoi, namespaces=fsroot.nsmap)
        if not fsRoiElList:
            print "Could not find ROI '%s' in the FS assessor XML"%fsRoi

            if leftRight:
                fsRoi = leftRightName + "-" + fsRoi
            else:
                sys.exit("Cannot determine if I should look for a left or right FS ROI from ILP ROI '%s'\nExiting"%ilpRoi)

            print "Checking for FS ROI '%s'"%fsRoi
            fsRoiElList = fsroot.xpath(volumeXpath,roiname=fsRoi, namespaces=fsroot.nsmap)
            if not fsRoiElList:
                sys.exit("Could not find ROI '%s' in the FS assessor XML\nExiting"%fsRoi)
    else:
        fsRoiElList = fsroot.xpath(surfaceXpath, leftRight=leftRight, roiname=fsRoi, namespaces=fsroot.nsmap)
        if not fsRoiElList:
            sys.exit("Could not find ROI '%s' in the FS assessor XML\nExiting"%fsRoi)

    fsRoiEl = fsRoiElList[0]

    # Add the ILP stat
    ilpTitle = "ilp_%s"%("zScore" if zscore else "norm")
    existingIlpElList = fsRoiEl.xpath('fs:%s'%ilpTitle, namespaces=fsroot.nsmap)
    if existingIlpElList:
        oldstat = existingIlpElList[0].text
        existingIlpElList[0].text = stat
        if isSurf:
            print "Overwriting existing stat {0}={1} with new stat {0}={2} for FS surface ROI '{3} {4}'".format(ilpTitle,oldstat,stat,leftRight,fsRoi)
        else:
            print "Overwriting existing stat {0}={1} with new stat {0}={2} for FS volumetric ROI '{3}'".format(ilpTitle,oldstat,stat,fsRoi)

        if len(existingIlpElList) > 1:
            for extraIlp in existingIlpElList[1:]:
                fsRoiEl.remove(extraIlp)
                if args.debug: print "Removing superfulous '%s' element"%ilpTitle
    else:
        etree.SubElement(fsRoiEl, "{%s}%s"%(fsns,ilpTitle)).text = stat
        if isSurf:
            print "Adding stat %s=%s to FS surface ROI '%s %s'"%(ilpTitle,stat,leftRight,fsRoi)
        else:
            print "Adding stat %s=%s to FS volumetric ROI '%s'"%(ilpTitle,stat,fsRoi)
    print
print "Done\n"

# Write out finished ILP assessor
print 'Writing assessor XML to %s'%args.outputXML
with open(args.outputXML,'w') as f:
    f.write(etree.tostring(fsxml, pretty_print=True, encoding='UTF-8', xml_declaration=True))
print "Done"
