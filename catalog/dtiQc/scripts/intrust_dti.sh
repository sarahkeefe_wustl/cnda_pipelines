#!/bin/bash

# if too few arguments, print version number and exit
if test -z "$3"; then
  echo 1.0
  exit
fi

#exit on failure
set -e
set +x

# input parameters

project=$1
session=$2
sessLabel=$3
user=$4
pass=$5
host=$6
scan=$7

# dependencies
source $SCRIPTS_HOME/slicer3_setup.sh
source $SCRIPTS_HOME/magicScalarFileConvert_setup.sh
source $SCRIPTS_HOME/fsl_setup.sh
source $SCRIPTS_HOME/ImageMagick_setup.sh

	dicom_directory="DICOM/$scan"  # directory of dicom files
	output_directory="$scan"  # directory for output files
	
	echo "SCAN: $scan"
	
	date '+%c'

	## create output directory
	mkdir -p $output_directory

	mkdir -p $dicom_directory

	restPath=${host}REST/experiments/$session/scans/$scan/resources/DICOM/files?format=zip
	echo $restPath
	
	zipfile="scan${scan}.zip"
	echo $zipfile
	
	curl -k -u ${user}:${pass} -G "$restPath" > $zipfile

	unzip -oj $zipfile -d $dicom_directory

	touch "$output_directory/1.nhdr"

	# convert images to NRRD format for processing - ~ 5 minutes
	DicomToNrrdConverter --inputDicomDirectory $dicom_directory --outputDirectory $output_directory --outputVolume "1.nhdr"

	# check for obliqueness
	if grep -q "space directions: (2,0,0) (0,2,0) (0,0,2)" $output_directory/1.nhdr
	then
		oblique="0"
	else
		oblique="1"
	fi
	
	## change to output directory
	cd $output_directory
	
	date '+%c'; echo "tend estim"

	# tensor estimation, get info from NRRD header key/value pairs, estimate B=0 value ~ 20 seconds
	tend estim -B kvp -knownB0 false -i "1.nhdr" -o "dti.nhdr"

	date '+%c'; echo done; echo tend anvol;

	# apply anisotropy metric (Basser/Pierpaoli fractional anisotropy), quantize to 8 bits, map 0 to 0, 1 to max; 
	# brighten image a little, pad image to an even number of slices, tile images, output to fa.png
	tend anvol -a fa -i "dti.nhdr" \
	 | unu quantize -b 8 -min 0 -max 1 \
	 | unu gamma -g 1.1 \
	 | unu pad -min 0 0 0 -max M M 79 -b pad \
	 | unu tile -a 2 0 1 -s 10 8 -o "fa.png"

	echo done ...... tend unmf

	# apply measurement frame included in the file to transform coordinates
	# make an RGB volume; use given fractional anisotropy; color principal eigenvector;
	# pad image to an even number of slices; tile images;
	# quantize to 8 bi ts, brighten image a little, output to fa-rgb.png
	tend unmf -i "dti.nhdr"\
	 | tend evecrgb -a fa -c 0\
	 | unu pad -min 0 0 0 0 -max M M M 79 -b pad\
	 | unu tile -a 3 1 2 -s 10 8\
	 | unu quantize -b 8 -min 0 -max 1\
	 | unu gamma -g 1.1 -o "fa-rgb.png"

	# apply anisotropy metric and save image
	tend anvol -a fa -i "dti.nhdr" -o "fa.nhdr"

	# create a histogram with 256-bin resolution, create an image of this histogram with height 256
	unu histo -b 256 -i "fa.nhdr"\
	 | unu dhisto -h 256 -o "fa-hist.png"

	# apply a different anisotropy metric and save image
	tend anvol -a tr -i "dti.nhdr" -o "tr.nhdr"

	# create a histogram with 256-bin resolution; create an image of this histogram
	unu histo -b 256 -i "tr.nhdr"\
	 | unu dhisto -h 256 -o "tr-hist.png"

	# convert from NRRD to g-zipped Nifti format
	magicScalarFileConvert fa.nhdr fa.nii.gz
	magicScalarFileConvert tr.nhdr tr.nii.gz

	fslmaths fa -thr 0.1 fa-thr # threshold- zero anything below 0.1 and output to fa-thr.nii.gz
	fslmaths fa-thr -bin mask       # binarize

	fastats=`fslstats fa -M -S -R`            # output mean for non-zero voxels; stddev for non-zerovoxels
	echo $fastats

	famaskstats=`fslstats fa -k mask -M -S -R`    # use "mask" for masking, repeat above
	echo $famaskstats

	trstats=`fslstats tr -M -S -R`            # same as above but for the other anisotropy metric
	echo $trstats

	trmaskstats=`fslstats tr -k mask -M -S -R`
	echo $trmaskstats

	touch xnatXML.xml
	
cat << EOF > xnatXML.xml
<?xml version="1.0" encoding="UTF-8"?>
<xnat:QCAssessment ID="${session}_DTI_QC" project="$project" label="${sessLabel}_DTI_QC" type="WUSTL_DTI_QC" xmlns:xnat="http://nrg.wustl.edu/xnat" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://nrg.wustl.edu/xnat https://cnda.wustl.edu/schemas/xnat/xnat.xsd">
	<xnat:date>`date "+%Y-%m-%d"`</xnat:date>
	<xnat:imageSession_ID>$session</xnat:imageSession_ID>
	<xnat:scans>
		<xnat:scan id="$scan">
			<xnat:scanStatistics xsi:type="xnat:statisticsData">
				<xnat:additionalStatistics name="oblique">`echo $oblique`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="fa_mean">`echo $fastats | cut -d' ' -f1`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="fa_sd">`echo $fastats | cut -d' ' -f2`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="fa_min">`echo $fastats | cut -d' ' -f3`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="fa_max">`echo $fastats | cut -d' ' -f4`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="famask_mean">`echo $famaskstats | cut -d' ' -f1`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="famask_sd">`echo $famaskstats | cut -d' ' -f2`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="famask_min">`echo $famaskstats | cut -d' ' -f3`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="famask_max">`echo $famaskstats | cut -d' ' -f4`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="tr_mean">`echo $trstats | cut -d' ' -f1`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="tr_sd">`echo $trstats | cut -d' ' -f2`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="tr_min">`echo $trstats | cut -d' ' -f3`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="tr_max">`echo $trstats | cut -d' ' -f4`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="trmask_mean">`echo $trmaskstats | cut -d' ' -f1`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="trmask_sd">`echo $trmaskstats | cut -d' ' -f2`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="trmask_min">`echo $trmaskstats | cut -d' ' -f3`</xnat:additionalStatistics>
				<xnat:additionalStatistics name="trmask_max">`echo $trmaskstats | cut -d' ' -f4`</xnat:additionalStatistics>
			</xnat:scanStatistics>
		</xnat:scan>
	</xnat:scans>
</xnat:QCAssessment>
EOF
	
	# need to get subject ID for REST path
	subjPath="${host}REST/projects/${project}/subjects?columns=ID,label&format=csv"
        echo path to get subjects is  $subjPath
	subject_ID=`curl -k -u ${user}:${pass} -G $subjPath | tail -1 | cut -d'"' -f4`
        curl -k -u ${user}:${pass} -G $subjPath | tail -1 | cut -d'"' -f4
        echo subject ID is $subject_ID
	
	restPath=${host}REST/projects/${project}/subjects/${subject_ID}/experiments/${session}/assessors/${session}_DTI_QC
	echo $restPath
	curl -k -u ${user}:${pass} -T xnatXML.xml $restPath

	restPath=${restPath}/out/resources

	curl -k -u ${user}:${pass} -T "fa.png" "$restPath/QC_FA/files/$scan/fa.png?inbody=true"
	curl -k -u ${user}:${pass} -T "fa-rgb.png" "$restPath/QC_FA-RGB/files/$scan/fa-rgb.png?inbody=true"
	curl -k -u ${user}:${pass} -T "fa-hist.png" "$restPath/QC_FA-HIST/files/$scan/fa-hist.png?inbody=true"
	curl -k -u ${user}:${pass} -T "tr-hist.png" "$restPath/QC_TR-HIST/files/$scan/tr-hist.png?inbody=true"

	#snapshots are huge, shrink them down for the website
	convert -thumbnail 300 fa.png fathumb.png
	convert -thumbnail 300 fa-rgb.png fa-rgbthumb.png

	curl -k -u ${user}:${pass} -T "fathumb.png" "$restPath/QC_FA/files/$scan/fathumb.png?inbody=true"
	curl -k -u ${user}:${pass} -T "fa-rgbthumb.png" "$restPath/QC_FA-RGB/files/$scan/fa-rgbthumb.png?inbody=true"

	date '+%c'

	echo $pwd	

    #Clean up
	#\rm -rf $dicom_directory
	#\rm $zipfile	

	exit 0
