#!/bin/tcsh -f

set FREESURFER_HOME=/nrgpackages/tools/freesurfer5

if ( ! $?FSLDIR ) then
  source /data/CNDA/pipeline/scripts/fsl5_setup.csh
endif

source ${FREESURFER_HOME}/SetUpFreeSurfer.csh

set QA_SCRIPTS=$FREESURFER_HOME/QAtools
set RECON_CHECKER_SCRIPTS=$QA_SCRIPTS/data_checker
set path=(${RECON_CHECKER_SCRIPTS} ${QA_SCRIPTS} ${path})
