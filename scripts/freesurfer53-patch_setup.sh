#!/bin/bash

PACKAGES_DIR=/nrgpackages
FREESURFER_HOME=${PACKAGES_DIR}/tools/freesurfer53-patch

if [ -z "$FSLDIR" ]; then
  source ${PACKAGES_DIR}/scripts/fsl5_setup.sh
fi
source ${FREESURFER_HOME}/SetUpFreeSurfer.sh
QA_SCRIPTS=$FREESURFER_HOME/QAtools
RECON_CHECKER_SCRIPTS=$QA_SCRIPTS/data_checker
PATH=$RECON_CHECKER_SCRIPTS:$QA_SCRIPTS:$PATH
export PATH FREESURFER_HOME
